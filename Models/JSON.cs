﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace WDGRestSession.Models
{
    public class Attribute
    {
        public int iVal { get; set; }
        public int attribute { get; set; }
        public string sVal { get; set; }
    }

    public class Databas
    {
        public string dbName { get; set; }
        public List<Attribute> attributes { get; set; }
    }

    public class License
    {
        public int enabled { get; set; }
        public string licenseid { get; set; }
        public long expireDt { get; set; }
        public string companyName { get; set; }
        public string companyContact { get; set; }
        public string companyEmail { get; set; }
        public int trial { get; set; }
        public List<Databas> databases { get; set; }
    }

    public class Object
    {
        public int enabled { get; set; }
        public License license { get; set; }
        public string email { get; set; }
    }

    public class LicenseRootObjet
    {
        public int status { get; set; }
        public string message { get; set; }
        public Object @object { get; set; }

        public static LicenseRootObjet ReadToObject(string json)
        {
            LicenseRootObjet deserializedUser = new LicenseRootObjet();
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
            DataContractJsonSerializer ser = new DataContractJsonSerializer(deserializedUser.GetType());
            deserializedUser = ser.ReadObject(ms) as LicenseRootObjet;
            ms.Close();
            return deserializedUser;
        }
    }

    public class SessionRootObject
    {
        public string email { get; set; }
        public string password { get; set; }

        public static String ReadToJSON(SessionRootObject sessionRootObject)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(SessionRootObject));
            MemoryStream stream = new MemoryStream();
            ser.WriteObject(stream, sessionRootObject);
            string jsonString = Encoding.UTF8.GetString(stream.ToArray());
            stream.Close();
            return jsonString;
        }
    }

    public class DvUserDTO
    {
        public int id { get; set; }
        public string description { get; set; }
        public int dialect { get; set; }
        public string email { get; set; }
        public int enabled { get; set; }
        public object lastPwdchangeDt { get; set; }
        public object loginErrors { get; set; }
        public string mphone { get; set; }
        public string pager { get; set; }
        public object password { get; set; }
        public string phone { get; set; }
        public string printerDefDocDriver { get; set; }
        public string printerDefDocPort { get; set; }
        public string printerDefDocPrinter { get; set; }
        public string printerDefLabelDriver { get; set; }
        public string printerDefLabelPort { get; set; }
        public string printerDefLabelPrinter { get; set; }
        public string printerDefSpLabelDriver { get; set; }
        public string printerDefSpLabelPort { get; set; }
        public string printerDefSpLabelPrinter { get; set; }
        public object shipmentId { get; set; }
        public int status { get; set; }
        public int type { get; set; }
        public string userid { get; set; }
        public object warehouseId { get; set; }
        public int userGrpId { get; set; }
    }

    public class SessionResponseRootObject
    {
        public string sessionId { get; set; }
        public DvUserDTO dvUserDTO { get; set; }

        public static SessionResponseRootObject ReadToObject(string json)
        {
            SessionResponseRootObject deserializedUser = new SessionResponseRootObject();
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
            DataContractJsonSerializer ser = new DataContractJsonSerializer(deserializedUser.GetType());
            deserializedUser = ser.ReadObject(ms) as SessionResponseRootObject;
            ms.Close();
            return deserializedUser;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WDGRestSession.Models;

namespace WDGRestSession
{
    public partial class Main : Form
    {
        private static readonly HttpClient client = new HttpClient();      
        public delegate void TextBoxDelegate(TextBox tb, String text);
        public delegate void ComboBoxDelegate(ComboBox cb, List<Databas> databases);

        public Main()
        {
            InitializeComponent();
        }

        private async void btnGetLicense_Click(object sender, EventArgs e)
        {        

            try
            {
                HttpResponseMessage response = await client.GetAsync(tbServerURL.Text + tbEmail.Text);
                response.EnsureSuccessStatusCode();    // Throw if not a success code.
                String responseString = await client.GetStringAsync(tbServerURL.Text + tbEmail.Text).ConfigureAwait(false);
                LicenseRootObjet ro = LicenseRootObjet.ReadToObject(responseString);
                String license = ro.@object.license.licenseid;             
                tbLicense.BeginInvoke(new TextBoxDelegate(UpdateTextBox), new System.Object[] { tbLicense, license });
                cmdDatabase.BeginInvoke(new ComboBoxDelegate(UpdateComboBox), new System.Object[] { cmdDatabase, ro.@object.license.databases });
            }
            catch (HttpRequestException ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }
        
        public void UpdateTextBox(Control tb, String text)
        {
            tb.Text = text;
        }

        public void UpdateComboBox(ComboBox cb, List<Databas> databases)
        {
            cb.Items.Clear();
            if (databases.Count > 0)
            {
                foreach (Databas db in databases)
                {
                    cb.Items.Add(db.dbName);
                }
                cb.SelectedIndex = 0;
            }                            
        }

        private async void btnCreateSession_Click(object sender, EventArgs e)
        {
            try
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
                "Basic",
                Convert.ToBase64String(
                System.Text.ASCIIEncoding.ASCII.GetBytes(
                string.Format("{0}:{1}", tbEmail.Text, tbPassword.Text))));           
            SessionRootObject sessionRootOject = new SessionRootObject();
            sessionRootOject.email = tbEmail.Text;
            sessionRootOject.password = tbPassword.Text;

                HttpResponseMessage response = await client.PostAsync(tbSessionURL.Text, new StringContent(SessionRootObject.ReadToJSON(sessionRootOject), Encoding.UTF8, "application/json"));
                String json = await response.Content.ReadAsStringAsync();
                SessionResponseRootObject sessionResponse = SessionResponseRootObject.ReadToObject(json);
                tbSessionKey.BeginInvoke(new TextBoxDelegate(UpdateTextBox), new System.Object[] { tbSessionKey, sessionResponse.sessionId });
            }
            catch (HttpRequestException ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
            
            
        }

        private void cmdDatabase_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbSessionURL.Text = tbSessionPrefix.Text + tbLicense.Text + "/" + cmdDatabase.SelectedItem + "/session";        
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
